var express = require('express');
var _ = require('underscore');
var querystring = require('querystring');

/**
 * Create an express application instance
 */
var app = express();

/**
 * Create a Parse ACL which prohibits public access.  This will be used
 *   in several places throughout the application, to explicitly protect
 *   Parse User, TokenRequest, and TokenStorage objects.
 */
var restrictedAcl = new Parse.ACL();
restrictedAcl.setPublicReadAccess(false);
restrictedAcl.setPublicWriteAccess(false);

/**
 * Global app configuration section
 */
//app.set('views', 'cloud/views');  // Specify the folder to find templates
app.set('view engine', 'ejs');    // Set the template engine
app.use(express.bodyParser());    // Middleware for reading request body

//app.get('/test', function(req, res) {
//    //res.render('login', {});
//    res.json({test:true});
//});

app.get('/settings', function(req, res) {
    res.end("insert settings page here");
});
app.get('/callback', function(req, res) {
    res.end("insert settings page here");
});
app.post('/callback', function(req, res) {
    res.end("insert settings page here");
});
app.get('/manage', function(req, res) {
    res.end("insert settings page here");
});

// Attach the Express app to your Cloud Code
app.listen();