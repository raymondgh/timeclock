$( document ).ready(function() {
    $("#clockin").click(function(){
        $("#clockin").removeClass("clock-active");
        $("#clockout").addClass("clock-active");
        $("#clockout-summary").show();
    });

    $("#clockout").click(function(){
        $("#clockin").addClass("clock-active");
        $("#clockout").removeClass("clock-active");
        $("#clockout-summary").hide();
    });
});